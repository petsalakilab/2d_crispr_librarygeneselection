######################################################################################################################################################################################################
anchor_breast<-read.csv("./Tables/Table10.csv")$Gene 
anchor_colorectal<-read.csv("./Tables/Table11.csv")$Gene
Candidate_Breast<-read.csv("./Tables/Table8.csv")$x
Candidate_Colorectal<-read.csv("./Tables/Table9.csv")$x
######################################################################################################################################################################################################
All<-read.csv("./Tables/Table23.csv")
set1<-intersect(which(All$Organism.Interactor.A==9606),which(All$Organism.Interactor.A==9606))#human
set2<-union(union(which(All$Experimental.System=="Synthetic Growth Defect"),which(All$Experimental.System=="Synthetic Lethality")),which(All$Experimental.System=="Negative Genetic"))
set3<-intersect(which(All$Organism.Interactor.A==559292),which(All$Organism.Interactor.A==559292))#yeast
Human_SL<-All[intersect(set1,set2),]
Yeast_SL<-All[intersect(set3,set2),]

library('org.Hs.eg.db')
symbols1<-c('AKT1','AR','ARID1A','ARID1B','ATM','ATR','BARD1','BCL2','BCL2L1','BRCA1','BRCA2','BRD4','CDK12','CDK4','CDK6','CDK7','CHEK1','CHEK2','DNMT1','DNMT3B','EZH2','FGFR1','FGFR2','GLS','KMT2D','MCL1','MTOR','MYC','PARP1','PIK3CA','PIK3CB','PRMT1','PRMT5','PTPN11','RAD21','RAD51','RAF1','TAZ','YAP1','WEE1') #Breast anchors
breast_map<-mapIds(org.Hs.eg.db, symbols1, 'ENTREZID', 'SYMBOL')
symbols2<-c('AKT1','AKT2','APC','ARID1A','ATM','ATR','AURKA','BCL2','BCL2L1','BRAF','BRD4','CDK4','CDK6','CDK8','CHEK1','CTNNB1','DNMT1','DNMT3B','EGFR','ERBB2','IGF1R','JAK1','KRAS','MAP2K1','MAP2K2','MAPK1','MCL1','MDM2','MYC','PARP1','PIK3CA','PORCN','PRMT1','PRMT5','PTPN11','RAF1','SMAD4','SRC','TAZ','VEGFA','WEE1','WRN','YAP1') #Colorectal anchors
colorectal_map<-mapIds(org.Hs.eg.db, symbols2, 'ENTREZID', 'SYMBOL')

SL_colorectal<-list()
SL_breast<-list()
all_SL_breast<-c()
all_SL_colorectal<-c()

for (i in 1:40){
  print(i)
  SL_breast[[i]]<-union(Human_SL$Official.Symbol.Interactor.B[which(Human_SL$Entrez.Gene.Interactor.A==breast_map[i])],Human_SL$Official.Symbol.Interactor.A[which(Human_SL$Entrez.Gene.Interactor.B==breast_map[i])])
  all_SL_breast<-c(all_SL_breast,SL_breast[[i]])
}
unique(all_SL_breast)
intersect(unique(all_SL_breast),Candidate_Breast)

for (i in 1:43){
  print(i)
  SL_colorectal[[i]]<-union(Human_SL$Official.Symbol.Interactor.B[which(Human_SL$Entrez.Gene.Interactor.A==colorectal_map[i])],Human_SL$Official.Symbol.Interactor.A[which(Human_SL$Entrez.Gene.Interactor.B==colorectal_map[i])])
  all_SL_colorectal<-c(all_SL_colorectal,SL_colorectal[[i]])
}
unique(all_SL_colorectal)
intersect(unique(all_SL_colorectal),Candidate_Colorectal)


######################################################################################################################################################################################################
Orthologs<-read.csv("./Tables/Table24.csv")
x<-character(6532)
y<-character(6532)
library(org.Hs.eg.db) 
library(org.Sc.sgd.db)

for (i in 1:6532){
  ENTREZ1<-select(org.Hs.eg.db,keys = as.character(Orthologs$Gene1_uniprot_id[i]),columns = c("ENTREZID", "UNIPROT"),keytype = "UNIPROT")$ENTREZID
  ENTREZ2<-select(org.Sc.sgd.db,keys = as.character(Orthologs$Gene2_uniprot_id[i]),columns = c("ENTREZID", "UNIPROT"),keytype = "UNIPROT")$ENTREZID
  SYMBOL1<-select(org.Hs.eg.db,keys = ENTREZ1,columns = c("ENTREZID", "SYMBOL"),keytype = "ENTREZID")$SYMBOL
  x[i]<-SYMBOL1
  y[i]<-ENTREZ2
  print(i)
}

Orthologs$x<-x
Orthologs$y<-y


ort_counter<-matrix(0,nrow=460328,ncol=2)
for (i in 1:460328){
  E1<-Yeast_SL$Entrez.Gene.Interactor.A[i]
  E2<-Yeast_SL$Entrez.Gene.Interactor.B[i]
  ort_counter[i,1]<-length(which(Orthologs$y==E1))
  ort_counter[i,2]<-length(which(Orthologs$y==E2))
  print(i)
}

A<-numeric(855672)
B<-numeric(855672)
count<-0
for (i in which((ort_counter[,1]*ort_counter[,2])!=0)){
  E1<-Yeast_SL$Entrez.Gene.Interactor.A[i]
  E2<-Yeast_SL$Entrez.Gene.Interactor.B[i]
  S1<-which(Orthologs$y==E1)
  S2<-which(Orthologs$y==E2)
  for (i1 in 1:length(S1)){
    x1<-Orthologs$x[S1[i1]]
    for (i2 in 1:length(S2)){
      x2<-Orthologs$x[S2[i2]]
      count<-count+1
      A[count]<-x1
      B[count]<-x2
    }
  }
  print(i)
}


SL_colorectal2<-list()
SL_breast2<-list()
all_SL_breast2<-c()
all_SL_colorectal2<-c()

for (i in 1:40){
  print(i)
  SL_breast2[[i]]<-intersect(unique(union(B[which(A==anchor_breast$Gene[i])],A[which(B==anchor_breast$Gene[i])])),Breast_Genes)
  all_SL_breast2<-c(all_SL_breast2,SL_breast2[[i]])
}
unique(all_SL_breast2)
intersect(unique(all_SL_breast2),Candidate_Breast)


for (i in 1:43){
  print(i)
  SL_colorectal2[[i]]<-intersect(unique(union(B[which(A==anchor_colorectal$Gene[i])],A[which(B==anchor_colorectal$Gene[i])])),Colorectal_Genes)
  all_SL_colorectal2<-c(all_SL_colorectal2,SL_colorectal2[[i]])
}
unique(all_SL_colorectal2)
intersect(unique(all_SL_colorectal2),Candidate_Colorectal)

